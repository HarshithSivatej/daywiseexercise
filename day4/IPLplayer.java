import java.util.Scanner;
class CustomException extends Exception
{
CustomException(String h)
{
super(h);
}
}
public class IPLplayer
{
public static void main(String args[])
{
Scanner c= new Scanner(System.in);
System.out.println("Enter Player Name: ");
String name= c.nextLine();
System.out.println("Enter Age: ");
int age= c.nextInt();
try
{
if(age<19)
throw new CustomException("CustomException:InvalidAgeRangeException");
else
{
System.out.println("Player Name: "+name);
System.out.println("Player Age: "+age);
}
}
catch(CustomException e)
{
System.out.print(e.getMessage());
}
}	
}
