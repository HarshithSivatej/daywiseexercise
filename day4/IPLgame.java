import java.util.Scanner;
class MyException extends Exception
{	
MyException(String h)
{
super(h);
}
}
public class IPLgame
{
public static void main(String args[])
{
Scanner c= new Scanner(System.in);
String team= "Chennai Super Kings Deccan Chargers Delhi Daredevils Kings XI Punjab Kolkata Knight Riders Mumbai Indians Rajasthan Royals Royal Challengers Bangalore";
System.out.println("Enter winner team: ");
String winner= c.nextLine();
System.out.println("Enter runner Team: ");
String runner= c.nextLine();
try
{
if(!(team.contains(winner) && team.contains(runner)))
{
throw new MyException("TeamNameNotFoundException:Entered team is not a part of IPL Season 4");
}
else
{
System.out.println("Expected IPL Season 4 winner: "+winner);
System.out.println("Expected IPL Season 4 runner: "+runner);
}
}
catch(MyException e)
{
System.out.print(e.getMessage());
}
}
}
